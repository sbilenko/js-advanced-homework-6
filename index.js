/*
    Асинхронність - це коли наступна операція не чекає завершення попередньої. Іншими словами, це можливість виконувати декілька операцій паралельно.
*/

const button = document.querySelector('.btn')
const info = document.querySelector('#result')

async function getIP(ip) {
    const response = await fetch(ip)
    const result = await response.json()
    return result
}

async function findByIP() {
    const ipResult = await getIP('http://api.ipify.org/?format=json')
    const ip = ipResult.ip
    
    const locationResult = await getIP(`http://ip-api.com/json/${ip}`)
    const continent = locationResult.timezone.split('/')[0]
    const country = locationResult.country
    const region = locationResult.region
    const city = locationResult.city
    const district = locationResult.regionName

    info.innerHTML = `Континет: ${continent} <br> Країна: ${country} <br> Регіон: ${region} <br> Місто: ${city} <br> Район: ${district}`
}

button.addEventListener('click', () => {
    findByIP()
})








/* const button = document.querySelector('.btn')

// Функція для виконання AJAX запиту
async function fetchData(url) {
    const response = await fetch(url)
    const data = await response.json()
    return data
}

// Функція для обробки результатів запиту
async function findIP() {
    const ipResponse = await fetchData('http://api.ipify.org/?format=json')
    const ip = ipResponse.ip

    const locationResponse = await fetchData(`http://ip-api.com/json/${ip}`)
    console.log(locationResponse)
    const continent = locationResponse.continent
    const country = locationResponse.country
    const region = locationResponse.region
    const city = locationResponse.city
    const district = locationResponse.regionName

    // Виведення результатів на сторінку
    const resultElement = document.getElementById('result')
    resultElement.innerHTML = `Континент: ${continent}<br>Країна: ${country}<br>Регіон: ${region}<br>Місто: ${city}<br>Район: ${district}`

}
 */
// button.addEventListener('click', await findIP())

